#ifndef ABSTRACTGRAPH_H
#define ABSTRACTGRAPH_H

#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/unordered/unordered_map.hpp>
#include <boost/graph/depth_first_search.hpp>

#include <QDebug>

#include "QtLogger/Logger.h"
#include "alphanumerical.h"

/* To access properties of edges and vertices with
  a property map */
enum vertex_properties_t { vertex_properties };
enum edge_properties_t { edge_properties };
namespace boost {
        BOOST_INSTALL_PROPERTY(vertex, properties);
        BOOST_INSTALL_PROPERTY(edge, properties);
}
/******************************************************/

using namespace boost;
using namespace boost::unordered;
using namespace std;

template <typename VertexProperties, typename EdgeProperties>
class AbstractGraph
{
public:

    typedef adjacency_list<setS, listS, /*directedS*/bidirectionalS,
        property<vertex_properties_t, VertexProperties, property<vertex_index_t, int> >,
        property<edge_properties_t, EdgeProperties> > GraphContainer;

    typedef typename graph_traits<GraphContainer>::vertex_descriptor Vertex;
    typedef typename graph_traits<GraphContainer>::edge_descriptor Edge;

    typedef typename graph_traits<GraphContainer>::vertex_iterator vertex_iter;
    typedef typename graph_traits<GraphContainer>::edge_iterator edge_iter;
    typedef typename graph_traits<GraphContainer>::adjacency_iterator adjacency_iter;

    typedef typename graph_traits<GraphContainer>::out_edge_iterator out_edge_iter;
    typedef typename graph_traits<GraphContainer>::in_edge_iterator in_edge_iter;

    typedef typename graph_traits<GraphContainer>::degree_size_type degree_t;

    typedef pair<adjacency_iter, adjacency_iter> adjacency_vertex_range_t;

    typedef pair<out_edge_iter, out_edge_iter> out_edge_range_t;
    typedef pair<in_edge_iter, in_edge_iter> in_edge_range_t;

    typedef pair<vertex_iter, vertex_iter> vertex_range_t;
    typedef pair<edge_iter, edge_iter> edge_range_t;

    typedef map<string, Vertex, naturalcmp_less<string> > VertexMap;
    typedef typename VertexMap::iterator VertexMapIterator;

    AbstractGraph():m_index(0){}

    //AbstractGraph(AbstractGraph& g) :
    //    graph(g.graph){}

    virtual ~AbstractGraph(){}

    void clear()
    {
        graph.clear();
    }

    Vertex addVertex(const VertexProperties prop)
    {
        if(!vertexExists(prop.id()))
        {
            Vertex v = add_vertex(graph);
            typename property_map<GraphContainer, vertex_properties_t>::type
                    map = get(vertex_properties, graph);
            put(map, v, prop);
            typename property_map<GraphContainer, vertex_index_t>::type
                    index_map = get(vertex_index, graph);
            put(index_map, v, m_index++);

            verticesMap.insert(pair<string, Vertex>(prop.id(), v));
            verticesMap2.insert(pair<string, Vertex>(prop.nid(), v));
            return v;
        }
        else
        {
            return verticesMap[prop.id()];
        }

    }

    void removeVertex(const Vertex& v)
    {
        clear_vertex(v, graph);
        remove_vertex(v, graph);
        verticesMap.erase(properties(v).id());
    }

    Edge addEdge(const Vertex& v1, const Vertex& v2, const EdgeProperties prop)
    {
        if(!edgeExists(prop.id()))
        {
            Edge e = add_edge(v1, v2, graph).first;
            typename property_map<GraphContainer, edge_properties_t>::type
                    map = get(edge_properties, graph);
            put(map, e, prop);
            edgesMap.insert(pair<string, Edge>(prop.id(), e));
            return e;
        }
        else
            return edgesMap[prop.id()];
    }

    VertexProperties& properties(const Vertex &v)
    {
        typename property_map<GraphContainer, vertex_properties_t>::type
                param = get(vertex_properties, graph);        
        return param[v];
    }

    const VertexProperties& properties(const Vertex &v) const
    {
        typename property_map<GraphContainer, vertex_properties_t>::const_type
                param = get(vertex_properties, graph);
        return param[v];
    }

    EdgeProperties& properties(const Edge& e)
    {
        typename property_map<GraphContainer, edge_properties_t>::type param = get(edge_properties, graph);
        return param[e];
    }

    const EdgeProperties& properties(const Edge& e) const
    {
        typename property_map<GraphContainer, edge_properties_t>::const_type param = get(edge_properties, graph);
        return param[e];
    }

    const Vertex &getVertexById(string id)
    {
        typename VertexMap::iterator it = verticesMap.find(id);
        if(verticesMap.end() != it)
            return it->second;
        throw out_of_range("No vertex found");
    }

    const Vertex &getVertexByNId(string id)
    {
        typename VertexMap::iterator it = verticesMap2.find(id);
        if(verticesMap.end() != it)
            return it->second;
        throw out_of_range("No vertex found");
    }

    const Edge &getEdgeById(std::string id) const
    {
        return edgesMap.find(id)->second;
    }

    const GraphContainer& getGraph()const
    {
        return graph;
    }

    bool vertexExists(std::string id) const
    {
        if(verticesMap.find(id) != verticesMap.end())
            return true;
        else
            return false;
    }

    bool edgeExists(std::string id) const
    {
        if(edgesMap.find(id) != edgesMap.end())
            return true;
        else
            return false;
    }

    vertex_range_t getVertices() const
    {
        return vertices(graph);
    }

    const VertexMap getOrderedVertices() const
    {
        return verticesMap;
    }

    out_edge_range_t getOutEdges(Vertex& v) const
    {
        return out_edges(v, graph);
    }

    adjacency_vertex_range_t getAdjacentVertices(const Vertex& v) const
    {
        return adjacent_vertices(v, graph);
    }

    unsigned int getVertexCount() const
    {
        return num_vertices(graph);
    }

    unsigned int getVertexDegree(const Vertex& v) const
    {
        return in_degree(v, graph) + out_degree(v, graph);
    }

    unsigned int getVertexOutDegree(const Vertex& v) const
    {
        return out_degree(v, graph);
    }

    unsigned int getVertexInDegree(const Vertex& v)const
    {
        return in_degree(v, graph);
    }

    VertexMapIterator getTarget(Edge eit)
    {
        Vertex v = target(eit, graph);
        return verticesMap.find(this->properties(v).id());
    }

    /*AbstractGraph& operator=(AbstractGraph &rhs)
    {
        graph = rhs.graph;
        return *this;
    }*/

    void pretty_print()
    {
        vertex_iter i, end;
        out_edge_iter ei, edge_end;
        for (tie(i,end) = vertices(graph); i != end; ++i)
        {
            QString out;
            VertexProperties vp = properties(*i);
            out += vp.customProperties("form") + " (" + QString::fromStdString(vp.id()) + ") ";
            for (tie(ei,edge_end) = out_edges(*i, graph); ei != edge_end; ++ei)
            {
                VertexProperties vp = properties(target(*ei, graph));
                out += " --> " + vp.customProperties("form") + "(" + QString::fromStdString(vp.id()) + ")  ";
            }
            qDebug() << out;
        }
    }

protected:
    GraphContainer graph;
    VertexMap verticesMap;
    VertexMap verticesMap2;
    int m_index;
    unordered_map<string, Edge> edgesMap;
};

#endif // ABSTRACTGRAPH_H
