#ifndef GRAPHSSCENE_H
#define GRAPHSSCENE_H

#include <string>
#include <QList>
#include <QHash>
#include <QGraphicsScene>

#include "graphwidgetelements.h";

using namespace std;

class GraphsScene: public QGraphicsScene
{
    Q_OBJECT
public:
    GraphsScene(QObject *parent = 0);

    void prepareGraphDrawing();
    Node* drawNode(QStringList info, int level, string id, string nid, GraphWrapper *p);
    void changeTextOnNode(QStringList info, Node *n);

    Edge* drawEdge(QStringList info, Node* src, Node* target, std::string id, GraphWrapper *p);
    void drawFilename(QString filename);
    void clear();


private:
    int node_width, node_height, graph_pos, _level;
};

#endif // GRAPHSSCENE_H
