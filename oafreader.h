#ifndef OAFREADER_H
#define OAFREADER_H

#include <QList>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QSet>

class OAFReader
{
public:
    OAFReader();
    QSet<QString> read(QString path, QString filename);
};

#endif // OAFREADER_H
