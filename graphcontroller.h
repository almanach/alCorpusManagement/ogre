#ifndef GRAPHCONTROLLER_H
#define GRAPHCONTROLLER_H

#include "graphsscene.h"
#include "graphwrapper.h"
#include "oafwriter.h"
#include "oafreader.h"
#include "featuresstructurewidget.h"

#include <QObject>
#include <QHash>
#include <QString>

class GraphController: public QObject
{
    Q_OBJECT
public:
    GraphController(GraphsScene *sc, QObject *parent = 0);
    virtual ~GraphController();

    QGraphicsScene* scene() const{return _scene;}

    void createGraph(QString filepath);
    GraphWrapper *getGraph(QString filepath);
    vector<pair<unordered_set<string>, GraphWrapper *> > createSelection(bool save = true);

    QString generateRule(bool &ok);

    void clearScene();

private slots:
    void makeWidgetProperties();

private:
    void generateGraph(GraphWrapper *m, QSet<QString> annotations);
private:
    GraphsScene * _scene;
    QHash<QString, GraphWrapper*> graphs;
};

#endif // GRAPHCONTROLLER_H
