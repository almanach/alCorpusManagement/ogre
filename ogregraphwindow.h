#ifndef ORIONGRAPHWINDOW_H
#define ORIONGRAPHWINDOW_H

#include <QMainWindow>
#include "graphcontroller.h"
#include "config.h"

namespace Ui {
class OGREGraphWindow;
}

class OGREGraphWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit OGREGraphWindow(QWidget *parent = 0);
    ~OGREGraphWindow();

private slots:
    void addPackage();
    void removeTreeItem();
    void viewGraph();
    void viewGraph(QTreeWidgetItem* item, int column);
    void saveAnnotations();

    void openGraphs();
    void openDirWithGraphs();

    void generateRules();
    void enableGeneration();
    
private:
    void recurseViewGraph(QTreeWidgetItem *item);
    QString getPackageNameWithDir(QFileInfo fi);
private:
    Ui::OGREGraphWindow *ui;
    GraphController *g_controller;
    QHash<QString, QString> filename2filepath;
    Config *config;
};

#endif // ORIONGRAPHWINDOW_H
