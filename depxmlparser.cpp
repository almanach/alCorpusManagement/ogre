#include "depxmlparser.h"
#include <QtGui>


DepXMLParser::DepXMLParser(GraphType &g)
    :AbstractParser(g)
{
}

bool DepXMLParser::read(const QString fname)
{
    QFile *f = new QFile(fname);
    if(!f->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    parser.setDevice(f);
    if (parser.readNextStartElement())
    {
        if (parser.name() == "dependencies")
            readDepXML();
        else
            parser.raiseError(QObject::tr("The file seems to be a proper DepXML file."));
    }
    return !parser.error();
}

void DepXMLParser::readDepXML()
{
    Q_ASSERT(parser.isStartElement() && parser.name() == "dependencies");

    while( !(parser.tokenType() == QXmlStreamReader::EndElement
             && parser.name() == "dependencies" ))
    {
        QXmlStreamReader::TokenType token = parser.readNext();
        if(token == QXmlStreamReader::StartElement)
        {
            if(parser.name() == "node")
                readNode();
            else if(parser.name() == "edge")
                readEdge();
            else if(parser.name() == "op")
                readOp();
        }
        if(parser.hasError())
        {
            qDebug() << parser.errorString();
            qDebug() << parser.lineNumber();
            break;
        }
    }
}

void DepXMLParser::readNode()
{
    Q_ASSERT(parser.isStartElement() && parser.name() == "node");
    QString token   = parser.attributes().value("form").toString();
    QString lemma   = parser.attributes().value("lemma").toString();
    QString cat     = parser.attributes().value("cat").toString();
    QString nid = parser.attributes().value("id").toString();
    QString cid = parser.attributes().value("cluster").toString();

    if(token.isEmpty())
    {
        token = "___";
    }

    SimpleVertexProperties vp(cid.toStdString(), nid.toStdString());
    vp.setCustomProperties("form", token);
    vp.setCustomProperties("cat", cat);
    vp.setCustomProperties("lemma", lemma);
    vp.setDefaultVarFeat("cat");
    vp.setCatName("cat");

    SimpleGraph &g = boost::get<SimpleGraph>(graph);    
    g.addVertex(vp);
    nidTocid.insert(nid, cid);
}

void DepXMLParser::readEdge()
{
    Q_ASSERT(parser.isStartElement() && parser.name() == "edge");
    QString source = parser.attributes().value("source").toString();
    QString target = parser.attributes().value("target").toString();
    QString label  = parser.attributes().value("label").toString();
    QString eid    = parser.attributes().value("id").toString();

    if(nidTocid.contains(source) && nidTocid.contains(target))
    {
        std::string src_id = nidTocid.value(source).toStdString();
        std::string tar_id = nidTocid.value(target).toStdString();

        SimpleGraph &g = boost::get<SimpleGraph>(graph);
        SimpleEdgeProperties ep(eid.toStdString());
        ep.setCustomProperties("label", label);
        ep.setDefaultVarFeat("label");
        g.addEdge(g.getVertexById(src_id), g.getVertexById(tar_id), ep);
    }

    parser.readNextStartElement();
    if(parser.name() == "deriv")
    {
        QString source_op = parser.attributes().value("source_op").toString();
        QString target_op = parser.attributes().value("target_op").toString();
        opidTonid.insert(source_op, nidTocid.value(source));
        opidTonid.insert(target_op, nidTocid.value(target));
    }
}

void DepXMLParser::readOp()
{
    Q_ASSERT(parser.isStartElement() && parser.name() == "op");

    QString opid = parser.attributes().value("id").toString();
    QSet<QString> features;
    SimpleGraph &g = boost::get<SimpleGraph>(graph);

    try
    {
        SimpleVertexProperties& vp = g.properties(g.getVertexById(opidTonid.value(opid).toStdString()));
        while( !(parser.tokenType() == QXmlStreamReader::EndElement
                     && parser.name() == "op" ))
        {
            QXmlStreamReader::TokenType token = parser.readNext();
            if(token == QXmlStreamReader::StartElement)
            {
                if(parser.name() == "f")
                {
                    QString fname  = parser.attributes().value("name").toString();
                    QString fvalue = "";
                    parser.readNextStartElement();
                    if(parser.name() == "plus")
                        fvalue = "+";
                    else if(parser.name() == "val")
                        fvalue = parser.readElementText().trimmed();
                    if(!fvalue.isEmpty())
                        features.insert(fname+SimpleVertexProperties::sep+fvalue);
                }
            }
            if(parser.hasError())
                break;
        }
        vp = vp.unite(features);
    }
    catch(std::exception &e)
    {
        LOG_WARNING(opid + " " + opidTonid.value(opid));
        LOG_WARNING(QString::fromStdString(e.what()));
    }
}
