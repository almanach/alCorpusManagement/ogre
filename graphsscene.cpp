#include "graphsscene.h"

GraphsScene::GraphsScene(QObject *parent)
    :QGraphicsScene(parent)
{
    graph_pos = -50;
}

void GraphsScene::prepareGraphDrawing()
{
    graph_pos -= node_height + 10 + _level * 23;
    node_height = 0;
    node_width  = 0;
    _level = 0;
}

void GraphsScene::changeTextOnNode(QStringList info, Node *n)
{
    QString html = "<html><head>"
                    +QString("<style>i{color:blue;}u{color:red;}</style>")
                    +QString("</head><body>")
                    +QString("<b>"+info[0]+"</b><br /><i>"+info[1]+"</i><br /><u>"+info[2]+"</u>")
                    +QString("</body></html>");
    n->setHtml(html);
}

Node* GraphsScene::drawNode(QStringList info, int level, std::string id, std::string nid, GraphWrapper *p)
{
    _level = level;
    Node* node = new Node(level, id, nid, p);

    QString html = "<html><head>"
                    +QString("<style>i{color:blue;}u{color:red;}</style>")
                    +QString("</head><body>")
                    +QString("<b>"+info[0]+"</b><br /><i>"+info[1]+"</i><br /><u>"+info[2]+"</u>")
                    +QString("</body></html>");
    node->setHtml(html);
    node->setPos(node_width, graph_pos);
    addItem(node);
    node_width += node->boundingRect().width() + 20;
    node_height = qMax(node_height, (int)node->boundingRect().height());

    return node;
}

Edge* GraphsScene::drawEdge(QStringList info, Node *src, Node *target, string id, GraphWrapper *p)
{
    Edge* edge = new Edge(src, target, info[0], id, p);
    addItem(edge);
    return edge;
}

void GraphsScene::drawFilename(QString filename)
{
    QGraphicsTextItem *fileItem = new QGraphicsTextItem(filename);
    fileItem->setPos(0, graph_pos+node_height+20);
    addItem(fileItem);
}

void GraphsScene::clear()
{
    QGraphicsScene::clear();
    this->setSceneRect(QRectF());
    graph_pos = -50;
    emit selectionChanged();
}
