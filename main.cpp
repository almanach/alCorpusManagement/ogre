#include <QtGui/QApplication>
#include <exception>

#include "QtLogger/Logger.h"
#include "QtLogger/FileAppender.h"
#include "QtLogger/ConsoleAppender.h"
#include "ogregraphwindow.h"

const QString SimpleVertexProperties::sep = "%SEP%";

class MyApplication : public QApplication
{
public:
    MyApplication(int& argc, char ** argv) :
        QApplication(argc, argv) { }
    virtual ~MyApplication() { }

    // reimplemented from QApplication so we can throw exceptions in slots
    virtual bool notify(QObject * receiver, QEvent * event) {
        try {
            return QApplication::notify(receiver, event);
        } catch(std::exception& e) {
            qCritical() << "Exception thrown:" << e.what();
        }
        return false;
    }
};

int main(int argc, char *argv[])
{
    MyApplication a(argc, argv);

    MyApplication::setOrganizationName("Cocophotos");
    MyApplication::setOrganizationDomain("cocophotos.org");
    MyApplication::setApplicationName("OGRE");

    /*FileAppender* fileAppender = new FileAppender();
    fileAppender->setFileName(QApplication::applicationDirPath()+"/log_orion.txt");
    Logger::registerAppender(fileAppender);*/

    //Logger's creation
    ConsoleAppender *cAppender = new ConsoleAppender();
    //cAppender->setDetailsLevel(Logger::Error);
    Logger::registerAppender(cAppender);

    OGREGraphWindow w;
    w.show();
    return a.exec();
}
