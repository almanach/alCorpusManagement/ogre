#include "grqlmaker.h"

GrQLMaker::GrQLMaker()
{
    m_nodeVarIt = 0;
    m_edgeVarIt = 0;
}

QString GrQLMaker::makeRule(GraphWrapper *g)
{
    QString indent = "    ";
    QString final_rule = "rule{\n";
    GraphWrapper::NodesIterator it = g->nodesIteratorReset();
    while(m_visited.size() != g->numVertices())
    {
        if(g->numOutEdges(it) > 0 && g->numInEdges(it) == 0)
        {
            dfs(g, it);
        }
        ++it;
    }
    final_rule += indent + "match\n"+ indent +"{\n" + indent + indent + m_rule + "\n"+ indent +"}\n";
    final_rule += indent + "commands\n"+ indent +"{\n"+ indent +"}\n";
    final_rule += indent + "marks\n"+ indent +"{\n"+ indent +"}\n";
    final_rule += indent + "constraints\n" + indent + "{\n"+ indent +"}\n}";
    return final_rule;
}

void GrQLMaker::dfs(GraphWrapper *g, GraphWrapper::NodesIterator it)
{
    m_visited.insert(g->nodeProperty(it, "id").toStdString());
    m_rule += createNode(g, it);
    if(g->numOutEdges(it) > 1)
        m_rule += " {\n";
    for(GraphWrapper::EdgesIterator eit = g->edgesIteratorReset(it); eit != g->edgesIteratorEnd(it); ++eit)
    {
        if(m_visited.find(g->nodeProperty(g->targetNode(eit), "id").toStdString()) == m_visited.end())
        {
            m_rule += createEdge(g, eit);
            dfs(g, g->targetNode(eit));
        }
    }
    if(g->numOutEdges(it) > 1)
    {
        m_rule = m_rule.left(m_rule.size() - 2);
        m_rule += "\n}";
    }
    if(g->numOutEdges(it) == 0)
        m_rule += ",\n";
}

QString GrQLMaker::createNode(GraphWrapper* g, GraphWrapper::NodesIterator nit)
{
    QString rule = g->nodeVarName(nit)+QString::number(m_nodeVarIt);
    m_nodeVarIt++;
    bool first = true;
    //Get properties in the proper way for the current node
    QSet<QString> properties = g->getNodeProperties(nit);

    if(properties.size() > 0)
    {
        rule += "[";
        foreach(QString p, properties)
        {
            QString new_p = p.replace("%SEP%", ":\"");
            if(!first)
                rule += ", " + new_p + "\"";
            else
                rule += new_p + "\"";
            first = false;
        }
        rule += "]";
    }
    return rule;
}

QString GrQLMaker::createEdge(GraphWrapper* g, GraphWrapper::EdgesIterator eit)
{
    QString rule = " -";
    bool first = true;
    //Get properties in the proper way for the current node
    QSet<QString> properties = g->getEdgeProperties(eit);

    if(properties.size() > 0)
    {
        rule += "[";
        foreach(QString p, properties)
        {
            QString new_p = p.replace("%SEP%", ":\"");
            if(!first)
                rule += ", " + new_p + "\"";
            else
                rule += new_p + "\"";
            first = false;
        }
        rule += "]-> ";
    }
    else
        rule += "> ";
    return rule;
}
