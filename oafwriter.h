#ifndef OAFWRITER_H
#define OAFWRITER_H

#include <QtGui>
class OAFWriter
{
public:
    OAFWriter();
    bool write(QString path, QString filename, QList<QStringList> annotations);
};

#endif // OAFWRITER_H
