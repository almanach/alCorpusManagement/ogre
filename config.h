#ifndef CONFIG_H
#define CONFIG_H

#include <QSettings>

class Config
{
public:
    static Config *getInstance ()
    {
        if (NULL == _singleton)
        {
            _singleton =  new Config;
        }
        return _singleton;
    }

    static void kill ()
    {
        if (NULL != _singleton)
        {
            delete _singleton;
            _singleton = NULL;
        }
    }

    void setDefaultDirectory(QString path);
    QString getDefaultDirectory()const;

    void setDefaultGeneralisationRules(QString filepath);
    QString getDefaultGeneralisationRules()const;
private:
    Config(){}
    ~Config(){}

private:
    static Config *_singleton;
    QSettings settings;
};

#endif // CONFIG_H
