#ifndef GRAPHWRAPPER_H
#define GRAPHWRAPPER_H

#include "depxmlparser.h"

#include <boost/tr1/unordered_set.hpp>
#include <boost/graph/isomorphism.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <vector>
#include <QGraphicsItem>

class GraphWrapper
{    
public:
    typedef SimpleGraph::Vertex Node;
    typedef SimpleGraph::Edge Edge;
    typedef SimpleGraph::VertexMap::iterator NodesIterator;
    typedef SimpleGraph::out_edge_iter EdgesIterator;

    GraphWrapper(QString filepath);
    GraphWrapper(QString filepath, SimpleGraph &g, unordered_set<string> ids);
    ~GraphWrapper();

    QString getFilepath()const {return _filepath;}

    //New API (1/04/2012)
    NodesIterator nodesIteratorReset(){ return _nodes.begin(); }
    NodesIterator nodesIteratorEnd() { return _nodes.end(); }
    EdgesIterator edgesIteratorReset(NodesIterator it){ return _graph.getOutEdges(it->second).first; }
    EdgesIterator edgesIteratorEnd(NodesIterator it){ return _graph.getOutEdges(it->second).second; }
    EdgesIterator edgesIteratorReset(Node n){ return _graph.getOutEdges(n).first; }
    EdgesIterator edgesIteratorEnd(Node n){ return _graph.getOutEdges(n).second; }

    NodesIterator targetNode(EdgesIterator eit){return _graph.getTarget(*eit);}    

    unsigned int numOutEdges(NodesIterator it)const{return _graph.getVertexOutDegree(it->second);}
    unsigned int numInEdges(NodesIterator it)const{return _graph.getVertexInDegree(it->second);}
    unsigned int numVertices()const{return _graph.getVertexCount();}

    QString nodeProperty(NodesIterator it, QString p);
    QString nodeProperty(Node n, QString p);
    QString nodeProperty(string id, QString p);

    QString edgeProperty(EdgesIterator it, QString p);

    QSet<QString> getNodeProperties(NodesIterator it){ return _graph.properties(it->second).setOfProperties();}
    QSet<QString> getEdgeProperties(EdgesIterator it){ return _graph.properties(*it).setOfProperties();}
    QHash<QString, QString> getNodeProperties(string id){return _graph.properties(_graph.getVertexById(id)).featuresStruct();}
    QHash<QString, QString> getHNodeProperties(NodesIterator it){return _graph.properties(it->second).featuresStruct();}
    void setNodeProperties(string id, QHash<QString, QString> features){_graph.properties(_graph.getVertexById(id)).setFeaturesStruct(features);}
    void setNodeProperties(NodesIterator it, QHash<QString, QString> features){_graph.properties(it->second).setFeaturesStruct(features);}

    vector<Node> getNodes()
    {
        vector<Node> v;
        boost::copy(_nodes | boost::adaptors::map_values, back_inserter(v));
        return v;
    }

    void intersection(Node n1, Node n2)
    {
        _graph.properties(n1) = _graph.properties(n1).intersection(_graph.properties(n2).setOfProperties());
    }

    void intersection(NodesIterator n1, NodesIterator n2)
    {
        _graph.properties(n1->second) = _graph.properties(n1->second).intersection(_graph.properties(n2->second).setOfProperties());
    }

    void intersection(Edge e1, Edge e2)
    {
        _graph.properties(e1) = _graph.properties(e1).intersection(_graph.properties(e2).setOfProperties());
    }

    void intersection(EdgesIterator e1, EdgesIterator e2)
    {        
        _graph.properties(*e1) = _graph.properties(*e1).intersection(_graph.properties(*e2).setOfProperties());
    }

    void intersection(NodesIterator n1, QSet<QString> set)
    {
        _graph.properties(n1->second) = _graph.properties(n1->second).intersection(set);
    }

    void intersection(string id, QSet<QString> set)
    {
        _graph.properties(_graph.getVertexById(id)) = _graph.properties(_graph.getVertexById(id)).intersection(set);
    }

    void substraction(NodesIterator n1, QSet<QString> set)
    {
        _graph.properties(n1->second) = _graph.properties(n1->second).substraction(set);
    }

    void substraction(string id, QSet<QString> set)
    {
        _graph.properties(_graph.getVertexById(id)) = _graph.properties(_graph.getVertexById(id)).substraction(set);
    }

    QString nodeVarName(NodesIterator nit)
    {
        QString var = _graph.properties(nit->second).customProperties(_graph.properties(nit->second).defaultVarFeat());
        if(var.isEmpty())
            return "x";
        else
            return var;
    }

    QString edgeVarName(EdgesIterator eit)
    {
        QString var = _graph.properties(*eit).customProperties(_graph.properties(*eit).defaultVarFeat());
        if(var.isEmpty())
            return "x";
        else
            return var;
    }

    QString categoryName()const{return _graph.properties(_graph.getOrderedVertices().begin()->second).catName();}

    void add_node(map<string, string> prop)
    {
        QHash<QString, QString> p;
        SimpleVertexProperties vp;
        for(map<string, string>::iterator it = prop.begin(); it != prop.end(); ++it)
        {
            if(it->first == "__var__")
                vp.setNId(it->second);
            else if(it->first == "__id__")
                vp.setId(it->second);
            else
                p.insert(QString::fromStdString(it->first), QString::fromStdString(it->second));
        }
        vp.setFeaturesStruct(p);
        _graph.addVertex(vp);
    }

    void add_edge(string src, string tar, map<string, string> prop)
    {
        QHash<QString, QString> p;
        SimpleEdgeProperties ep;
        for(map<string, string>::iterator it = prop.begin(); it != prop.end(); ++it)
        {
            if(it->first == "__var__")
                ep.setId(it->second);
            else
                p.insert(QString::fromStdString(it->first), QString::fromStdString(it->second));
        }
        ep.setFeaturesStruct(p);
        _graph.addEdge(_graph.getVertexById(src), _graph.getVertexById(tar), ep);
    }
    ///

    GraphWrapper* createSubgraph(unordered_set<string> ids);
    const SimpleGraph& getGraph()const {return _graph;}
    void print();

private:
    SimpleGraph _graph;
    GraphType _graph_type;
    SimpleGraph::VertexMap _nodes;
    QString _filepath;
    AbstractParser *parser;
};

#endif // GRAPHWRAPPER_H
