#include "graphmatcher.h"

bool operator== (SimpleVertexProperties const &v1, SimpleVertexProperties const &v2)
{
    QSet<QString> s1 = v1.setOfProperties() - v2.setOfProperties();
    if(s1.size() > 0)
    {
        QSet<QString> s2 = v2.setOfProperties() - v1.setOfProperties();
        if(s2.size() > 0)
            return false;
        else
            return true;
    }
    else
        return true;
}


bool operator== (SimpleEdgeProperties const &e1, SimpleEdgeProperties const &e2)
{
    QSet<QString> s1 = e1.setOfProperties() - e2.setOfProperties();
    if(s1.size() > 0)
    {
        QSet<QString> s2 = e2.setOfProperties() - e1.setOfProperties();
        if(s2.size() > 0)
            return false;
        else
            return true;
    }
    else
        return true;
}

GraphMatcher::GraphMatcher()
{
}

void GraphMatcher::match(GraphWrapper *g1, GraphWrapper *g2)
{
    typedef property_map<SimpleGraph::GraphContainer, vertex_properties_t>::const_type VertexPropertiesMap;
    typedef property_map<SimpleGraph::GraphContainer, edge_properties_t>::const_type EdgePropertiesMap;
    VertexPropertiesMap vp1 = get(vertex_properties, g1->getGraph().getGraph());
    VertexPropertiesMap vp2 = get(vertex_properties, g2->getGraph().getGraph());

    EdgePropertiesMap ep1 = get(edge_properties, g1->getGraph().getGraph());
    EdgePropertiesMap ep2 = get(edge_properties, g2->getGraph().getGraph());

    int number = qMin(g1->numVertices(), g2->numVertices());
    MatcherCallback<SimpleGraph::GraphContainer> m(number, g1, g2);
    boost::mcgregor_common_subgraphs_maximum( g1->getGraph().getGraph(),g2->getGraph().getGraph(), false, m,
                                     edges_equivalent(make_property_map_equivalent(ep1, ep2)).
                                     vertices_equivalent(make_property_map_equivalent(vp1, vp2)) );
}
