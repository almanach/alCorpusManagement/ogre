#ifndef GRAPHMATCHER_H
#define GRAPHMATCHER_H

#include "graphwrapper.h"
#include "boost/graph/mcgregor_common_subgraphs.hpp"

template <typename Graph>
struct MatcherCallback
{
    MatcherCallback(uint n, GraphWrapper *small, GraphWrapper *big)
        : m_number_stop(n), m_big(big), m_small(small){}

    template <typename CorrespondenceMapFirstToSecond,
              typename CorrespondenceMapSecondToFirst>
    bool operator()(CorrespondenceMapFirstToSecond correspondence_map_1_to_2,
                    CorrespondenceMapSecondToFirst correspondence_map_2_to_1,
                    typename graph_traits<Graph>::vertices_size_type subgraph_size)
    {
        Q_UNUSED(correspondence_map_2_to_1);

        if(m_number_stop == subgraph_size)
        {
            // Print out correspondences between vertices
            BGL_FORALL_VERTICES_T(vertex1, m_small->getGraph().getGraph(), Graph)
            {
                // Skip unmapped vertices
                if (get(correspondence_map_1_to_2, vertex1) != graph_traits<Graph>::null_vertex())
                {
                    qDebug() << m_small->getNodeProperties(m_small->nodeProperty(vertex1, "id").toStdString())
                             << "<->"
                             << m_big->getNodeProperties(m_big->nodeProperty(get(correspondence_map_1_to_2, vertex1), "id").toStdString());
                }
            }
            qDebug() << "----------------------------------";
        }
        return (true);
    }
private:
    uint m_number_stop;
    GraphWrapper *m_big, *m_small;
};

class GraphMatcher
{
public:
    GraphMatcher();

    static void match(GraphWrapper *g1, GraphWrapper *g2);

private:
};

#endif // GRAPHMATCHER_H
