#ifndef GRQLSYNTAXHIGHLIGHTER_H
#define GRQLSYNTAXHIGHLIGHTER_H

#include <QtGui/QSyntaxHighlighter>

class GrQLSyntaxHighlighter : public QSyntaxHighlighter
{
    public:
        GrQLSyntaxHighlighter(QTextDocument *parent = 0);

    protected:
        virtual void highlightBlock(const QString &text);

    private:
        struct HighlightingRule
        {
            QRegExp pattern;
            QTextCharFormat format;
        };
        QVector<HighlightingRule> highlightingRules;

        QTextCharFormat edgeFormat;
        QTextCharFormat keyfeaturesFormat;
        QTextCharFormat valuefeaturesFormat;
        QTextCharFormat commentFormat;
        QTextCharFormat varFormat;
};

#endif // GRQLSYNTAXHIGHLIGHTER_H
