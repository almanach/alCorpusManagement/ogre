# Input
HEADERS += QtLogger/AbstractAppender.h \
           QtLogger/AbstractStringAppender.h \
           QtLogger/ConsoleAppender.h \
           QtLogger/FileAppender.h \
           QtLogger/Logger.h
SOURCES += QtLogger/AbstractAppender.cpp \
           QtLogger/AbstractStringAppender.cpp \
           QtLogger/ConsoleAppender.cpp \
           QtLogger/FileAppender.cpp \
           QtLogger/Logger.cpp

