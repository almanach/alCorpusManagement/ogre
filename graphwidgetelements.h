#ifndef GRAPHWIDGETELEMENTS_H
#define GRAPHWIDGETELEMENTS_H

#include "graphwrapper.h"

#include <QtGui>
#include <boost/tr1/unordered_map.hpp>
#include <boost/tr1/unordered_set.hpp>
class Node;

class Edge : public QGraphicsItem
{
public:
    Edge(Node *sourceNode, Node *destNode, QString label, std::string id, GraphWrapper* p);

    Node *sourceNode() const;
    Node *destNode() const;

    void adjust();

    enum { Type = UserType + 2 };
    int type() const { return Type; }

    std::string id()const {return _id;}
    GraphWrapper *parent()const {return _parent;}

protected:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    QHash<QString, QPointF> calculateEdge(int rounded);

private:
    Node *source, *dest;

    QPointF sourcePoint;
    QPointF destPoint;
    QPointF topLeft;
    QPointF bottomRight;
    qreal arrowSize;
    QString label;

    //Distance between node and top of the edge
    const int edge_dist;

    std::string _id;
    GraphWrapper *_parent;
};

class Node : public QGraphicsTextItem
{
    Q_OBJECT
public:
    Node(int level, std::string id, string nid, GraphWrapper *p);

    void addEdge(Edge *edge);
    QList<Edge *> edges() const;

    enum { Type = UserType + 1 };
    int type() const { return Type; }

    void calculateForces();
    bool advance();
    QPointF getOptimalPoint(QPointF dist);

    std::string id()const {return _id;}
    std::string nid()const {return _nid;}
    GraphWrapper *parent()const{return _parent;}

    int getLevel()const {return level;}

signals:
    void doubleClicked();

protected:
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

private:
    QList<Edge *> edgeList;
    QPointF newPos;
    int level;
    std::string _id;
    std::string _nid;
    GraphWrapper *_parent;
};

/*inline bool operator==(const Node &n1, const Node &n2)
{
    return n1.id() == n2.id();
}

inline uint qHash(const Node &key)
{
    return qHash(QString::fromStdString(key.id()));
}*/


#endif // GRAPHWIDGETELEMENTS_H
