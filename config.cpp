#include "config.h"

Config *Config::_singleton = NULL;

void Config::setDefaultDirectory(QString path)
{
    settings.setValue("general/default_dir", path);
    settings.sync();
}

QString Config::getDefaultDirectory()const
{
    return settings.value("general/default_dir", "").toString();
}

void Config::setDefaultGeneralisationRules(QString filepath)
{
    settings.setValue("generalisation/rules", filepath);
    settings.sync();
}

QString Config::getDefaultGeneralisationRules()const
{
    return settings.value("generalisation/rules", "").toString();
}
