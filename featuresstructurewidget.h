#ifndef FEATURESSTRUCTUREWIDGET_H
#define FEATURESSTRUCTUREWIDGET_H

#include <QDialog>
#include <QHash>

namespace Ui {
class FeaturesStructureWidget;
}

class FeaturesStructureWidget : public QDialog
{
    Q_OBJECT
    
public:
    explicit FeaturesStructureWidget(QDialog *parent = 0);
    ~FeaturesStructureWidget();
    
    void fillTable(QHash<QString, QString> features);

    QHash<QString, QString> features()const{return m_properties;}

private slots:
    void save();
    void cancel();
    void addRow();
    void removeRow();
private:
    Ui::FeaturesStructureWidget *ui;
    QHash<QString, QString> m_properties;
};

#endif // FEATURESSTRUCTUREWIDGET_H
