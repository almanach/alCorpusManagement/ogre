#ifndef ALPHANUMERICAL_H
#define ALPHANUMERICAL_H

/*
The Alphanum Algorithm is an improved sorting algorithm for strings
containing numbers.  Instead of sorting numbers in ASCII order like a
standard sort, this algorithm sorts numbers in numeric order.

The Alphanum Algorithm is discussed at http://www.DaveKoelle.com

This implementation is Copyright (c) 2008 Dirk Jagdmann <doj@cubic.org>.
It is a cleanroom implementation of the algorithm and not derived by
other's works. In contrast to the versions written by Dave Koelle this
source code is distributed with the libpng/zlib license.

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you
       must not claim that you wrote the original software. If you use
       this software in a product, an acknowledgment in the product
       documentation would be appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and
       must not be misrepresented as being the original software.

    3. This notice may not be removed or altered from any source
       distribution.
In March 2012, this version has been corrected by C. RIBEYRE to
put in a project.
*/

#include <cassert>
#include <functional>
#include <string>
#include <sstream>


static bool alphanum_isdigit(const char c)
{
    return c>='0' && c<='9';
}

static int naturalsort_impl(const char *l, const char *r)
{
    enum mode_t { STRING, NUMBER } mode=STRING;

    while(*l && *r)
    {
        if(mode == STRING)
        {
            char l_char, r_char;
            while((l_char=*l) && (r_char=*r))
            {
                // check if this are digit characters
                const bool l_digit=alphanum_isdigit(l_char), r_digit=alphanum_isdigit(r_char);
                // if both characters are digits, we continue in NUMBER mode
                if(l_digit && r_digit)
                {
                    mode=NUMBER;
                    break;
                }
                // if only the left character is a digit, we have a result
                if(l_digit) return -1;
                // if only the right character is a digit, we have a result
                if(r_digit) return +1;
                // compute the difference of both characters
                const int diff=l_char - r_char;
                // if they differ we have a result
                if(diff != 0) return diff;
                // otherwise process the next characters
                ++l;
                ++r;
            }
        }
        else // mode==NUMBER
        {
            // get the left number
            unsigned long l_int=0;
            while(*l && alphanum_isdigit(*l))
            {
                // TODO: this can overflow
                l_int=l_int*10 + *l-'0';
                ++l;
            }
            // get the right number
            unsigned long r_int=0;
            while(*r && alphanum_isdigit(*r))
            {
                // TODO: this can overflow
                r_int=r_int*10 + *r-'0';
                ++r;
            }
            // if the difference is not equal to zero, we have a comparison result
            const long diff=l_int-r_int;
            if(diff != 0)
                return diff;
            // otherwise we process the next substring in STRING mode
            mode=STRING;
        }
    }

    if(*r) return -1;
    if(*l) return +1;
    return 0;
}

static int naturalsort(std::string l, std::string r)
{
    return naturalsort_impl(l.c_str(), r.c_str());
}

template<class Ty>
struct naturalcmp_less : public std::binary_function<Ty, Ty, bool>
{
    bool operator()(const Ty& left, const Ty& right) const
    {
        return naturalsort(left, right) < 0;
    }
};

#endif  //ALPHANUMERICAL_H
