#include "featuresstructurewidget.h"
#include "ui_featuresstructurewidget.h"

#include <QDebug>

FeaturesStructureWidget::FeaturesStructureWidget(QDialog *parent) :
    QDialog(parent),
    ui(new Ui::FeaturesStructureWidget)
{
    ui->setupUi(this);
    ui->featuresTable->setAlternatingRowColors(true);

    setWindowFlags(Qt::Tool);

    connect(ui->cancelFeatBtn, SIGNAL(clicked()), this, SLOT(cancel()));
    connect(ui->saveFeatBtn, SIGNAL(clicked()), this, SLOT(save()));
    connect(ui->addRowBtn, SIGNAL(clicked()), this, SLOT(addRow()));
    connect(ui->removeRowBtn, SIGNAL(clicked()), this, SLOT(removeRow()));
}

FeaturesStructureWidget::~FeaturesStructureWidget()
{
    delete ui;
}

void FeaturesStructureWidget::fillTable(QHash<QString, QString> features)
{
    m_properties = features;
    QHashIterator<QString, QString> it(features);
    while(it.hasNext())
    {
        it.next();
        ui->featuresTable->insertRow(ui->featuresTable->rowCount());

        QTableWidgetItem *keyItem = new QTableWidgetItem(it.key());
        ui->featuresTable->setItem(ui->featuresTable->rowCount()-1, 0, keyItem);
        ui->featuresTable->setItem(ui->featuresTable->rowCount()-1, 1, new QTableWidgetItem(it.value()));
    }
}

void FeaturesStructureWidget::save()
{
    m_properties.clear();
    for(int i = 0; i < ui->featuresTable->rowCount(); i++)
    {
        QTableWidgetItem * it0 = ui->featuresTable->item(i, 0);
        QTableWidgetItem * it1 = ui->featuresTable->item(i, 1);
        if(it0->text().isEmpty() || it1->text().isEmpty())
            continue;
        else
            m_properties.insert(it0->text(), it1->text());
    }
}

void FeaturesStructureWidget::cancel()
{
    this->close();
}

void FeaturesStructureWidget::addRow()
{
    ui->featuresTable->insertRow(ui->featuresTable->rowCount());
}

void FeaturesStructureWidget::removeRow()
{

    bool first = true;
    int rangeCount = 0;
    foreach(QTableWidgetSelectionRange range, ui->featuresTable->selectedRanges())
    {
        for(int i = range.topRow(); i < range.topRow()+range.rowCount(); i++)
        {
            if(first)
                ui->featuresTable->removeRow(range.topRow());
            else
                ui->featuresTable->removeRow(range.topRow()-rangeCount);
        }
        first = false;
        rangeCount = range.rowCount();
    }
}
