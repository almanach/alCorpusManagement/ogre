#ifndef ABSTRACTREADER_H
#define ABSTRACTREADER_H

#include <QString>
#include <QXmlStreamReader>
#include "genericgraph.h"

class AbstractParser
{
public:
    AbstractParser(GraphType &g): graph(g) {}
    virtual ~AbstractParser(){}
    virtual bool read(const QString fname) = 0;
    QString errorString() const
    {
        return QObject::tr("%1\nLine %2, column %3")
                     .arg(parser.errorString())
                     .arg(parser.lineNumber())
                     .arg(parser.columnNumber());
    }
protected:
    QXmlStreamReader parser;
    GraphType &graph;
};

#endif // ABSTRACTREADER_H
