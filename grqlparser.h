#ifndef GRQLPARSER_H
#define GRQLPARSER_H

#include <vector>
#include <map>
#include <utility>
#include <fstream>

#include <boost/mpl/print.hpp>
#include <boost/foreach.hpp>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/karma.hpp>

#include <boost/fusion/include/std_pair.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/container/map.hpp>
#include <boost/variant/recursive_variant.hpp>

//#include <graphwrapper.h>

//using namespace std;
//namespace qi      = boost::spirit::qi;
//namespace ascii   = boost::spirit::ascii;
//namespace fusion = boost::fusion;
//namespace phoenix = boost::phoenix;

//namespace grql
//{
//    struct nbinary;
//    struct ebinary;
//    struct ternary;
//    struct multiple;

//    typedef pair<boost::optional<string>, boost::optional<map<string, string> > > prim_type;
//    typedef boost::variant<
//        boost::recursive_wrapper<nbinary>,
//        boost::recursive_wrapper<ebinary>,
//        boost::recursive_wrapper<ternary>,
//        boost::recursive_wrapper<multiple>,
//        prim_type
//        > graph;

//    struct nbinary
//    {
//        prim_type n_obj;
//        vector<graph> g;
//    };

//    struct ebinary
//    {
//        prim_type e_obj;
//        vector<graph> g;
//    };

//    struct ternary
//    {
//        prim_type n_obj;
//        prim_type e_obj;
//        vector<graph> g;
//    };

//    struct multiple
//    {
//        prim_type obj;
//        vector<ebinary> g;
//    };
//}

//BOOST_FUSION_ADAPT_STRUCT(
//    grql::nbinary,
//    (grql::prim_type, n_obj)
//    (std::vector<grql::graph>, g)
//);

//BOOST_FUSION_ADAPT_STRUCT(
//    grql::ebinary,
//    (grql::prim_type, e_obj)
//    (std::vector<grql::graph>, g)
//);

//BOOST_FUSION_ADAPT_STRUCT(
//    grql::ternary,
//    (grql::prim_type, n_obj)
//    (grql::prim_type, e_obj)
//    (std::vector<grql::graph>, g)
//);

//BOOST_FUSION_ADAPT_STRUCT(
//    grql::multiple,
//    (grql::prim_type, obj)
//    (std::vector<grql::ebinary>, g)
//);


//namespace grql
//{
//    template<typename Iter>
//    class GrQLParser: public qi::grammar<Iter, vector<graph>(), ascii::space_type>
//    {
//        public:
//            GrQLParser()
//                : GrQLParser::base_type(start)
//            {
//                //Primitives
//                var    = qi::char_("A-Za-z_") >> *qi::char_("a-zA-Z_0-9");
//                fname  = +qi::char_("a-zA-Z_0-9*");
//                fvalue = qi::lexeme['"' >> +(qi::char_ - '"') >> '"'];

//                //Feature structure objects
//                feature       = fname >> ':' >> fvalue;
//                features_list = feature % ',';

//                //Graph Objects
//                node = ( var >> -('[' >> features_list >> ']') | qi::lit("[]"));
//                edge          = qi::lit("-->") | qi::lit("->") |
//                        ('-' >> -var >> -('[' >> features_list >> ']') >> qi::lit("->") );

//                //Relations
//                binary_rel   = node >> '<' >> ('(' >> relations >> ')' | relations);
//                ternary_rel  = node >> edge >> ('(' >> relations >> ')' | relations);
//                simple_rel   = edge >> ('(' >> relations >> ')' | relations);
//                multiple_rel = node >> '{' >> simple_rel % ',' >> '}';
//                relations    = '(' >> node >> ')' | binary_rel | ternary_rel | multiple_rel;

//                //Commands
//                command     = relabel | remove_edge | remove_node | add_node | add_edge | add_feature;
//                relabel     = qi::lit("relabel") >> '(' >> fvalue >> ',' >> var >> ')';
//                remove_edge = qi::lit("remove_edge") >> '(' >> var >> ')';
//                remove_node = qi::lit("remove_node") >> '(' >> var >> ')';
//                add_node    = qi::lit("add_node") >> '(' >> var >> ',' >> var >> ')';
//                add_edge    = qi::lit("add_edge") >> '(' >> var >> ',' >> var >> ')';
//                add_feature = qi::lit("add_feature") >> '(' >> fname >> ',' >> fvalue >> ',' >> var >> ')';

//                //Sub rules
//                matcher     = qi::lit("match") >> '{' >> relations >> '}';
//                commands    = qi::lit("commands") >> '{' >> -(command % ',') >> '}';
//                constraints = qi::lit("constaints") >> '{' >> qi::eps >> '}';
//                marks       = qi::lit("marks") >> '{' >> qi::eps >> '}';

//                //Rule and list of rules
//                subrule = -(marks >> constraints) | -(constraints >> marks);
//                rule    = matcher;//(matcher >> commands >> subrule) | (commands >> matcher >> subrule);
//                start   = rule % ',';
//            }

//        private:
//            qi::rule< Iter, vector<graph>(),  ascii::space_type > start;
//            qi::rule< Iter, graph(),          ascii::space_type > rule;
//            qi::rule< Iter,   ascii::space_type > subrule;

//            //Subsections in a rule
//            qi::rule< Iter, graph(), ascii::space_type > matcher;
//            qi::rule< Iter, ascii::space_type > commands;
//            qi::rule< Iter, ascii::space_type > marks;
//            qi::rule< Iter, ascii::space_type > constraints;

//            //Commands
//            qi::rule< Iter, ascii::space_type > command;
//            qi::rule< Iter, ascii::space_type > add_node;
//            qi::rule< Iter, ascii::space_type > add_edge;
//            qi::rule< Iter, ascii::space_type > add_feature;
//            qi::rule< Iter, ascii::space_type > remove_node;
//            qi::rule< Iter, ascii::space_type > remove_edge;
//            qi::rule< Iter, ascii::space_type > relabel;

//            //Relations
//            qi::rule< Iter, graph(),    ascii::space_type > relations;
//            qi::rule< Iter, nbinary(),  ascii::space_type > binary_rel;
//            qi::rule< Iter, ternary(),  ascii::space_type > ternary_rel;
//            qi::rule< Iter, ebinary(),  ascii::space_type > simple_rel;
//            qi::rule< Iter, multiple(), ascii::space_type > multiple_rel;

//            //Primitives
//            qi::rule<Iter, prim_type(),            ascii::space_type > node;
//            qi::rule<Iter, prim_type(),            ascii::space_type > edge;
//            qi::rule<Iter, map<string, string>(),  ascii::space_type > features_list;
//            qi::rule<Iter, pair<string, string>(), ascii::space_type > feature;
//            qi::rule<Iter, string(),               ascii::space_type > fname, fvalue, var;
//    };
//}

//using namespace grql;

//struct graph_maker: boost::static_visitor<>
//{
//    graph_maker(GraphWrapper* &g)
//        :m_graph(g){}

//    void extract_info(prim_type p)
//    {
//        string __var__ = "";
//        map<string, string> m;
//        if(p.first)
//            __var__ = boost::get<string>(p.first);
//        if(p.second)
//            m = boost::get<map<string, string> >(p.second);
//        m.insert(make_pair("__var__", __var__));
//    }

//    void operator()(prim_type const& node)const
//    {
//        extract_info(node);
//        cout << " ";
//    }

//    void operator()(nbinary const& binary_rel)const
//    {
//        extract_info(binary_rel.n_obj);
//        cout << " < ";
//        BOOST_FOREACH(graph const& subgraph, binary_rel.g)
//        {
//            boost::apply_visitor(graph_printer(), subgraph);
//        }
//        cout << endl;
//    }

//    void operator()(ebinary const& binary_rel)const
//    {
//        cout << " -";
//        extract_info(binary_rel.e_obj);
//        cout << "-> ";
//        BOOST_FOREACH(graph const& subgraph, binary_rel.g)
//        {
//            boost::apply_visitor(graph_printer(), subgraph);
//        }
//        cout << endl;
//    }

//    void operator()(ternary const& ternary_rel)const
//    {
//        extract_info(ternary_rel.n_obj);
//        cout << " -";
//        extract_info(ternary_rel.e_obj);
//        cout << "-> ";

//        BOOST_FOREACH(graph const& subgraph, ternary_rel.g)
//        {
//            boost::apply_visitor(graph_printer(), subgraph);
//        }
//        cout << endl;
//    }

//    void operator()(multiple const& multiple_rel)const
//    {
//        extract_info(multiple_rel.obj);
//        cout << "{" << endl;
//        BOOST_FOREACH(graph const& subgraph, multiple_rel.g)
//        {
//            boost::apply_visitor(graph_printer(), subgraph);
//        }
//        cout << "}" << endl;
//    }

//    GraphWrapper* &m_graph;
//};
#endif // GRQLPARSER_H
