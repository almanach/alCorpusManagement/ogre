#include "rulesguesser.h"
#include "config.h"

#include <boost/range/combine.hpp>
#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/assign.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/foreach.hpp>

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>
#include <boost/bind.hpp>

#include <fstream>

using namespace boost;
using namespace tuples;

namespace qi      = boost::spirit::qi;
namespace ascii   = boost::spirit::ascii;
namespace bfusion = boost::fusion;

template <typename Iterator>
struct generalization_language
  : qi::grammar<Iterator, ascii::space_type>
{
    generalization_language(GraphWrapper *&g)
        : generalization_language::base_type(structures), m_graph(g)
    {
        string sep = SimpleVertexProperties::sep.toStdString();
        m_catToNodes_created = false;

        //Rules instantiation
        structures      = features_struct % ',';
        features_struct = (name >> '[' >> features_set >> ']')[boost::bind(&generalization_language::generalize, this, _1)];
        features_set    = feature % ',';
        feature         = (name >> ':' >> value)[qi::_val = qi::_1+sep+qi::_2];
        value           = (qi::lexeme['"' >> +(qi::char_ - '"') >> '"'] | qi::char_('*'));
        name            = +qi::char_("a-zA-Z_0-9*");
    }

    void properties_reduce(GraphWrapper::NodesIterator it, QSet<QString> properties, QSet<QString> star_properties)
    {
        m_graph->substraction(it, properties);
        QHash<QString, QString> nodeProperties = m_graph->getHNodeProperties(it);
        foreach(QString p, star_properties)
        {
            QStringList p_list = p.split(SimpleVertexProperties::sep);
            if(nodeProperties.contains(p_list[0]))
                nodeProperties.remove(p_list[0]);
        }
        m_graph->setNodeProperties(it, nodeProperties);
    }

    void properties_reduce(string id, QSet<QString> properties, QSet<QString> star_properties)
    {
        m_graph->substraction(id, properties);
        QHash<QString, QString> nodeProperties = m_graph->getNodeProperties(id);
        foreach(QString p, star_properties)
        {
            QStringList p_list = p.split(SimpleVertexProperties::sep);
            if(nodeProperties.contains(p_list[0]))
                nodeProperties.remove(p_list[0]);
        }
        m_graph->setNodeProperties(id, nodeProperties);
    }

    void generalize(bfusion::vector<string, vector<string> > arg_)
    {
        vector<string> v = bfusion::at_c<1> (arg_);
        QSet<QString> properties;
        QSet<QString> star_properties;
        for(vector<string>::iterator it = v.begin(); it != v.end(); ++it)
        {
            QString p = QString::fromStdString(*it);
            if(p.contains(SimpleVertexProperties::sep+"*"))
                star_properties.insert(p);
            else
                properties.insert(p);
        }

        QString cat = QString::fromStdString(bfusion::at_c<0> (arg_));
        if(cat == "*")
        {
            //Iterate over all nodes
            for(GraphWrapper::NodesIterator it = m_graph->nodesIteratorReset(); it != m_graph->nodesIteratorEnd(); ++it)
            {
                properties_reduce(it, properties, star_properties);
                if(!m_catToNodes_created)
                {
                    QString node_cat    = m_graph->nodeProperty(it, m_graph->categoryName());
                    QSet<QString> nodes = m_catToNodes.value(node_cat, QSet<QString>());

                    nodes.insert(m_graph->nodeProperty(it, "id"));
                    m_catToNodes.insert(node_cat, nodes);
                }
            }
            m_catToNodes_created = true;
        }
        else
        {
            if(m_catToNodes_created)
            {
                foreach(QString qid, m_catToNodes.value(cat))
                {
                    string id = qid.toStdString();
                    properties_reduce(id, properties, star_properties);
                }
            }
            else
            {
                //Iterate over all nodes
                for(GraphWrapper::NodesIterator it = m_graph->nodesIteratorReset(); it != m_graph->nodesIteratorEnd(); ++it)
                {
                    if(m_graph->nodeProperty(it, m_graph->categoryName()) == cat)
                        properties_reduce(it, properties, star_properties);
                    if(!m_catToNodes_created)
                    {
                        QSet<QString> nodes = m_catToNodes.value(cat, QSet<QString>());
                        nodes.insert(m_graph->nodeProperty(it, "id"));
                        m_catToNodes.insert(cat, nodes);
                    }
                }
                m_catToNodes_created = true;
            }
        }
    }//end of generalize

    //Rules definition
    qi::rule<Iterator, ascii::space_type> structures;
    qi::rule<Iterator, pair<string, vector<string> >(), ascii::space_type > features_struct;
    qi::rule<Iterator, vector<string>(), ascii::space_type> features_set;
    qi::rule<Iterator, string(), ascii::space_type> feature;
    qi::rule<Iterator, string(), ascii::space_type> value, name;

    GraphWrapper* &m_graph;
    bool m_catToNodes_created;
    QHash<QString, QSet<QString> > m_catToNodes;
};

RulesGuesser::RulesGuesser(vector<pair<unordered_set<string>, GraphWrapper *> > graphs)
    :_graphs(graphs)
{

}

bool RulesGuesser::generalize(GraphWrapper* &g)
{
    //Recreate graph structures
    for(vector<pair<unordered_set<string>, GraphWrapper*> >::iterator it = _graphs.begin();
        it != _graphs.end(); ++it)
    {
        pair<unordered_set<string>, GraphWrapper*> p = *it;
        _subgraphs.push_back(p.second->createSubgraph(p.first));
    }
    //There is just one graph selected, so it's the least generalisation
    //we can get. Return it to make the rule.
    if(_subgraphs.size() == 1)
    {
        g = _subgraphs[0];
        return true;
    }

    //Compare graphs
    g = _subgraphs[0];
    bool ok = true;
    vector<GraphWrapper*>::iterator it = _subgraphs.begin();
    ++it;
    for(; it != _subgraphs.end(); ++it)
    {
        if(!cmp_graphs(g, *it))
        {
            ok = false;
            break;
        }
    }

    QString filename = Config::getInstance()->getDefaultGeneralisationRules();
    if(filename.isEmpty())
        LOG_WARNING("Error: there is no generalization file provided");
    else
    {
        //Try to load generalization file and over generalize the final graph
        ifstream in((filename.toStdString()).c_str(), ios_base::in);
        if (!in)
            LOG_WARNING("Error: Could not open input file: "+filename);
        else
        {
            string storage; // We will read the contents here.
            in.unsetf(ios::skipws); // No white space skipping!
            copy(istream_iterator<char>(in), istream_iterator<char>(), back_inserter(storage));
            string::iterator begin = storage.begin();
            string::iterator end = storage.end();

            generalization_language<string::iterator> p(g);

            if (!qi::phrase_parse(begin, end, p, ascii::space))
                LOG_WARNING("Parsing of generalization rules failed");
        }
    }
    return ok;
}

bool RulesGuesser::cmp_graphs(GraphWrapper* g1, GraphWrapper* g2)
{    
    if(g1->numVertices() != g2->numVertices())
    {
        LOG_ERROR("vertices are not the same");
        return false;
    }

    typedef tuple<GraphWrapper::NodesIterator,
                    GraphWrapper::NodesIterator> TupleNodesIterator;
    typedef tuple<GraphWrapper::EdgesIterator, GraphWrapper::EdgesIterator> TupleEdgesIterator;
    typedef zip_iterator<TupleNodesIterator> ZipNodesIterator;
    typedef zip_iterator<TupleEdgesIterator> ZipEdgesIterator;


    TupleNodesIterator t_begin = make_tuple(g1->nodesIteratorReset(), g2->nodesIteratorReset());
    TupleNodesIterator t_end   = make_tuple(g1->nodesIteratorEnd(), g2->nodesIteratorEnd());
    for(ZipNodesIterator it = make_zip_iterator(t_begin); it != make_zip_iterator(t_end); ++it)
    {
        TupleNodesIterator t = it.get_iterator_tuple();
        GraphWrapper::NodesIterator n1 = t.get<0>(), n2 = t.get<1>();
        g1->intersection(n1, n2);

        if(g1->numOutEdges(n1) != g2->numOutEdges(n2))
        {
            LOG_ERROR("Out edges are not the same, isomorphism is wrong.");
            return false;
        }

        typedef std::map<std::string, GraphWrapper::EdgesIterator, naturalcmp_less<std::string> > EdgesIteratorMap;
        typedef EdgesIteratorMap::iterator EdgesIteratorMapIterator;

        EdgesIteratorMap g1_emap, g2_emap;

        TupleEdgesIterator te_begin = make_tuple(g1->edgesIteratorReset(n1), g2->edgesIteratorReset(n2));
        TupleEdgesIterator te_end   = make_tuple(g1->edgesIteratorEnd(n1), g2->edgesIteratorEnd(n2));
        for(ZipEdgesIterator eit = make_zip_iterator(te_begin); eit != make_zip_iterator(te_end); ++eit)
        {
            TupleEdgesIterator te = eit.get_iterator_tuple();
            GraphWrapper::EdgesIterator e1 = te.get<0>(), e2 = te.get<1>();
            g1_emap.insert(make_pair(g1->nodeProperty(g1->targetNode(e1), "id").toStdString(), e1));
            g2_emap.insert(make_pair(g2->nodeProperty(g2->targetNode(e2), "id").toStdString(), e2));
        }

        typedef tuple<EdgesIteratorMapIterator, EdgesIteratorMapIterator> TupleEdgesIteratorMap;
        TupleEdgesIteratorMap b = make_tuple(g1_emap.begin(), g2_emap.begin());
        TupleEdgesIteratorMap e = make_tuple(g1_emap.end(), g2_emap.end());
        for(zip_iterator<TupleEdgesIteratorMap> eit = make_zip_iterator(b); eit != make_zip_iterator(e); ++eit)
        {
            TupleEdgesIteratorMap te = eit.get_iterator_tuple();
            GraphWrapper::EdgesIterator e1 = te.get<0>()->second, e2 = te.get<1>()->second;
            g1->intersection(e1, e2);
        }

    }
    return true;
}


