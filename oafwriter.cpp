#include "oafwriter.h"

#include <QtGui>

OAFWriter::OAFWriter()
{
}

bool OAFWriter::write(QString path,
                 QString filename,
                 QList<QStringList> annotations)
{
    QFile w(path+"/"+filename+".oaf");
    if(!w.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream w_stream(&w);
    for(int i = 0; i < annotations.size(); i++)
    {
        QString ann = annotations[i].join(":");
        w_stream << ann << "\n";
    }
    w.close();
    return true;
}
