#include "grqlsyntaxhighlighter.h"

GrQLSyntaxHighlighter::GrQLSyntaxHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    HighlightingRule rule;

    valuefeaturesFormat.setForeground(Qt::red);
    rule.pattern = QRegExp("\".+\"");
    rule.format  = valuefeaturesFormat;
    highlightingRules.append(rule);

    keyfeaturesFormat.setForeground(Qt::blue);
    keyfeaturesFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("(?:\\[|, )[a-zA-Z0-9]+:");
    rule.format = keyfeaturesFormat;
    highlightingRules.append(rule);

    varFormat.setForeground(Qt::black);
    rule.pattern = QRegExp("[A-za-z0-9_]+\\[");
    rule.format  = varFormat;
    highlightingRules.append(rule);

    edgeFormat.setForeground(Qt::darkBlue);
    edgeFormat.setFontWeight(QFont::Bold);
    rule.pattern = QRegExp("->|-\\[|\\]->|\\[|\\]");
    rule.format = edgeFormat;
    highlightingRules.append(rule);

    commentFormat.setForeground(Qt::darkGreen);
    rule.pattern = QRegExp("#.+$");
    rule.format  = commentFormat;
    highlightingRules.append(rule);
}

void GrQLSyntaxHighlighter::highlightBlock(const QString &text)
{
     foreach (const HighlightingRule &rule, highlightingRules) {
         QRegExp expression(rule.pattern);
         int index = text.indexOf(expression);
         while (index >= 0) {
             int length = expression.matchedLength();
             setFormat(index, length, rule.format);
             index = text.indexOf(expression, index + length);
         }
     }
     setCurrentBlockState(0);
}
