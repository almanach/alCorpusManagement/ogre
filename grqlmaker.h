#ifndef GRQLMAKER_H
#define GRQLMAKER_H

#include "graphwrapper.h"
#include <QString>



class GrQLMaker
{
public:
    GrQLMaker();
    QString makeRule(GraphWrapper *g);

private:
    void dfs(GraphWrapper *g, GraphWrapper::NodesIterator it);
    QString createNode(GraphWrapper *g, GraphWrapper::NodesIterator nit);
    QString createEdge(GraphWrapper *g, GraphWrapper::EdgesIterator eit);
private:
    unordered_set<std::string> m_visited;
    QString m_rule;
    unsigned int m_nodeVarIt;
    unsigned int m_edgeVarIt;
};

#endif // GRQLMAKER_H
