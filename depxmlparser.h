#ifndef DEPXMLPARSER_H
#define DEPXMLPARSER_H

#include "abstractparser.h"

class DepXMLParser: public AbstractParser
{
public:
    DepXMLParser(GraphType &g);
    bool read(const QString fname);

protected:
    void readDepXML();    
    void readNode();
    void readEdge();
    void readOp();

private:
    QHash<QString, QString> nidTocid;
    QHash<QString, QString> opidTonid;
};

#endif // DEPXMLPARSER_H
