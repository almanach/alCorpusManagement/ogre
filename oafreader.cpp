#include "oafreader.h"

#include <QtGui>
#include <exception>

OAFReader::OAFReader()
{
}

QSet<QString> OAFReader::read(QString path, QString filename)
{
    QFile r(path+"/"+filename+".oaf");
    //qDebug() << QFileInfo(r).absoluteFilePath();
    if(!r.open(QIODevice::ReadOnly | QIODevice::Text))
        return QSet<QString>();

    QSet<QString> annotations;

    while(!r.atEnd())
    {
        QByteArray line = r.readLine();
        QStringList annotation = QString(line).split(":");
        annotations << annotation[0]+"_"+annotation[1];
    }
    return annotations;
}
