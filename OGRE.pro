#-------------------------------------------------
#
# Project created by QtCreator 2012-03-07T13:43:23
#
#-------------------------------------------------

QT       += core gui xml

TARGET = OGRE
TEMPLATE = app
CONFIG -= app_bundle
INCLUDEPATH+=/Volumes/Documents/Universite/M2R_LI/Memoire/OGRE/boost149

SOURCES += main.cpp\
    depxmlparser.cpp \
    graphwidget.cpp \
    ogregraphwindow.cpp \
    graphwidgetelements.cpp \
    graphcontroller.cpp \
    graphsscene.cpp \
    oafwriter.cpp \
    oafreader.cpp \
    rulesguesser.cpp \
    graphwrapper.cpp \
    grqlmaker.cpp \
    grqlsyntaxhighlighter.cpp \
    featuresstructurewidget.cpp \
    config.cpp \
    graphmatcher.cpp \
    genericgraph.cpp \
    grqlparser.cpp

HEADERS  += \
    depxmlparser.h \
    abstractgraph.h \
    genericgraph.h \
    graphwidget.h \
    abstractparser.h \
    alphanumerical.h \
    ogregraphwindow.h \
    graphwidgetelements.h \
    graphcontroller.h \
    graphsscene.h \
    oafwriter.h \
    oafreader.h \
    rulesguesser.h \
    graphwrapper.h \
    grqlmaker.h \
    grqlsyntaxhighlighter.h \
    featuresstructurewidget.h \
    config.h \
    graphmatcher.h \
    grqlparser.h

include(QtLogger/QtLogger.pri)


UI_DIR = tmp/ui
MOC_DIR = tmp/moc
OBJECTS_DIR = tmp/obj
RCC_DIR = tmp/qrc

FORMS += \
    ogregraphwindow.ui \
    featuresstructurewidget.ui

RESOURCES += \
    OGRE.qrc
