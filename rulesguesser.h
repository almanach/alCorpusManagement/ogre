#ifndef RULESGUESSER_H
#define RULESGUESSER_H

#include <QList>
#include <QSet>
#include <QMap>
#include <vector>

#include "graphwrapper.h"

using namespace std;
using namespace boost;
class RulesGuesser
{
public:
    RulesGuesser(vector<pair<unordered_set<string>, GraphWrapper *> > graphs);
    bool generalize(GraphWrapper * &g);

private:
    bool cmp_graphs(GraphWrapper *g1, GraphWrapper *g2);

private:
    vector<pair<unordered_set<string>, GraphWrapper *> > _graphs;
    vector<GraphWrapper*> _subgraphs;
};

#endif // RULESGUESSER_H
