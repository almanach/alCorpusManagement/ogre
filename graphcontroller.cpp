#include "graphcontroller.h"

#include "rulesguesser.h"
#include "grqlmaker.h"
#include "graphmatcher.h"

using namespace std;

GraphController::GraphController(GraphsScene *sc, QObject *parent)
    :QObject(parent), _scene(sc)
{}

GraphController::~GraphController()
{}

void GraphController::generateGraph(GraphWrapper *m, QSet<QString> annotations)
{
    _scene->prepareGraphDrawing();
    QHash<QString, Node*> graphic_nodes;
    int level = 1;
    for(GraphWrapper::NodesIterator it = m->nodesIteratorReset(); it != m->nodesIteratorEnd(); ++it)
    {
        QString form  = m->nodeProperty(it, "form");
        QString lemma = m->nodeProperty(it, "lemma");
        QString cat   = m->nodeProperty(it, "cat");
        string nid    = m->nodeProperty(it, "nid").toStdString();

        if(form.isEmpty())
            form = "*";
        if(lemma.isEmpty())
            lemma = "*";
        if(cat.isEmpty())
            cat   = "*";

        Node *n = _scene->drawNode(QStringList() << form << lemma << cat, level++, it->first, nid, m);
        connect(n, SIGNAL(doubleClicked()), this, SLOT(makeWidgetProperties()));

        if(annotations.contains("node_"+QString::fromStdString(nid)))
            n->setSelected(true);
        graphic_nodes.insert(m->nodeProperty(it, "id"), n);
    }

    for(GraphWrapper::NodesIterator it = m->nodesIteratorReset(); it != m->nodesIteratorEnd(); ++it)
    {
        string id_src = it->first;
        Node *src = graphic_nodes.value(QString::fromStdString(id_src));

        for(GraphWrapper::EdgesIterator eit = m->edgesIteratorReset(it);
            eit != m->edgesIteratorEnd(it); ++eit)
        {
            QString id_tar = m->nodeProperty(m->targetNode(eit), "id");
            Node *tar = graphic_nodes.value(id_tar);

            QString label = m->edgeProperty(eit, "label");
            if(label.isEmpty())
                label = "*";

            string eid = m->edgeProperty(eit, "id").toStdString();
            Edge *e = _scene->drawEdge(QStringList() << label, src, tar, eid, m);
            if(annotations.contains("edge_"+QString::fromStdString(eid)))
                e->setSelected(true);
        }
    }
}

void GraphController::createGraph(QString filepath)
{
    OAFReader reader;
    QSet<QString> annotations = reader.read(QFileInfo(filepath).absolutePath(), QFileInfo(filepath).baseName());
    GraphWrapper *m = new GraphWrapper(filepath);
    generateGraph(m, annotations);
    _scene->drawFilename(QFileInfo(filepath).fileName());
    graphs.insert(filepath, m);
}

GraphWrapper* GraphController::getGraph(QString filepath)
{
    return graphs.value(filepath);
}

QString GraphController::generateRule(bool &ok)
{
    vector<pair<unordered_set<string>, GraphWrapper*> > select = createSelection(false);
    GraphWrapper *t = new GraphWrapper(qApp->applicationDirPath()+"/test_suj.dep.xml");//new GraphWrapper((select[0].second)->getFilepath());
    RulesGuesser *r = new RulesGuesser(select);
    GraphWrapper *general = 0;
    if(!r->generalize(general))
    {
        ok = false;
        return "";
    }
    else
    {
        ok = true;
        GrQLMaker *grql = new GrQLMaker();
        clearScene();
        generateGraph(general, QSet<QString>());

        GraphMatcher::match(general, t);

        return grql->makeRule(general);
    }
}

vector<pair<unordered_set<string>, GraphWrapper*> > GraphController::createSelection(bool save)
{
    OAFWriter oaf;
    QSet<QGraphicsItem*> items = _scene->selectedItems().toSet();
    QHashIterator<QString, GraphWrapper*> it(graphs);
    vector<pair<unordered_set<string>, GraphWrapper*> > data_rule;
    while(it.hasNext())
    {
        it.next();
        GraphWrapper *m = it.value();        
        unordered_set<string> ids;
        QList<QStringList> annotations;
        foreach(QGraphicsItem* item, items)
        {
            Edge *e;
            Node *n = qgraphicsitem_cast<Node*>(item);
            if(n == 0)
            {
                e = qgraphicsitem_cast<Edge*>(item);
                if(e->parent() == m)
                {
                    QString id   = QString::fromStdString(e->id());
                    QString type = "edge";
                    annotations << (QStringList() << type << id << "selected");
                }
            }
            else
            {
                if(n->parent() == m)
                {
                    QString id   = QString::fromStdString(n->nid());
                    QString type = "node";
                    if(type == "node")
                        ids.insert(id.toStdString());
                    annotations << (QStringList() << type << id << "selected");
                }
            }
        }

        if(save)
            oaf.write(QFileInfo(it.key()).absolutePath(), QFileInfo(it.key()).baseName(), annotations);
        if(ids.size() > 0)
            data_rule.push_back(make_pair<unordered_set<string>, GraphWrapper*>(ids, m));
    }
    return data_rule;
}

void GraphController::clearScene()
{
    _scene->clear();
    _scene->setSceneRect(QRectF());
    foreach(GraphWrapper *m, graphs)
    {
        delete m;
    }
    graphs.clear();
}

void GraphController::makeWidgetProperties()
{
    Node* node = qobject_cast<Node*>(sender());
    GraphWrapper *p = node->parent();

    //Draw the widget to edit properties (features)
    FeaturesStructureWidget *featWidget = new FeaturesStructureWidget();
    featWidget->fillTable(p->getNodeProperties(node->id()));
    featWidget->exec();

    //Update the features struct in vertex properties
    p->setNodeProperties(node->id(), featWidget->features());

    QString form  = p->nodeProperty(node->id(), "form");
    QString cat   = p->nodeProperty(node->id(), p->categoryName());
    QString lemma = p->nodeProperty(node->id(), "lemma");
    if(form.isEmpty())
        form = "*";
    if(cat.isEmpty())
        cat = "*";
    if(lemma.isEmpty())
        lemma = "*";

    _scene->changeTextOnNode(QStringList() << form << lemma << cat, node);
}
