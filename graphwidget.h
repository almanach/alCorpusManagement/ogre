#ifndef GRAPHWIDGET_H
#define GRAPHWIDGET_H

#include <QtGui>
#include "graphwidgetelements.h"
class Node;
class Edge;
class GraphicWidget;

class GraphWidget : public QGraphicsView
{
    Q_OBJECT

public:
    GraphWidget(QWidget *parent = 0);

    void itemMoved();

public slots:
    void zoomIn();
    void zoomOut();

protected:
    void keyPressEvent(QKeyEvent *event);
    void timerEvent(QTimerEvent *event);
    void scaleView(qreal scaleFactor);

private:
    int timerId;
    QHash<int, QSet<QGraphicsItem*> > graphs;

};
#endif // GRAPHWIDGET_H
