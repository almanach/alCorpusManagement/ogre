#ifndef GENERICGRAPH_H
#define GENERICGRAPH_H

#include "abstractgraph.h"
#include <string>
#include <QString>
#include <QSet>
#include <QStringList>
#include <QHash>
#include <boost/variant.hpp>

using namespace std;

//! SimpleGraph : a graph with really simple properties

class SimpleVertexProperties
{
public:
    enum Operations{Union, Intersection, Substraction};
    static const QString sep;

    SimpleVertexProperties(){}
    SimpleVertexProperties(string id, string nid)
        :m_id(id), m_nid(nid), m_properties(QHash<QString, QString>()){}

    void setId(string id){m_id = id;}
    string id()const {return m_id;}

    void setNId(string id){m_nid = id;}
    string nid()const {return m_nid;}

    void setDefaultVarFeat(QString featKey){m_defaultVarFeat = featKey;}
    QString defaultVarFeat()const {return m_defaultVarFeat;}

    void setCatName(QString catName){m_catName = catName;}
    QString catName()const {return m_catName;}

    void setCustomProperties(QString key, QString value){m_properties.insert(key, value);}
    QString customProperties(QString key)const {return m_properties.value(key, "");}
    QSet<QString> setOfProperties()const
    {
        QSet<QString> p;
        QHashIterator<QString, QString> it(m_properties);
        while(it.hasNext())
        {
            it.next();
            p.insert(it.key()+SimpleVertexProperties::sep+it.value());
        }
        return p;
    }

    QHash<QString, QString> featuresStruct() {return m_properties;}
    void setFeaturesStruct(QHash<QString, QString> features){m_properties = features;}

    SimpleVertexProperties& intersection(QSet<QString> set)
    {
       return operation(set, Intersection);
    }

    SimpleVertexProperties& unite(QSet<QString> set)
    {
        return operation(set, Union);
    }

    SimpleVertexProperties& substraction(QSet<QString> set)
    {
        return operation(set, Substraction);
    }

private:
    SimpleVertexProperties& operation(QSet<QString> set, Operations op)
    {
        QSet<QString> m_set  = this->setOfProperties();
        QSet<QString> result;
        switch(op)
        {
        case Union:
            result = m_set.unite(set);
            break;
        case Intersection:
            result = m_set.intersect(set);
            break;
        case Substraction:
            result = m_set.subtract(set);
            break;
        default:
            return *this;
        }

        m_properties.clear();
        foreach(QString p, result)
        {
            QStringList splitting = p.split(SimpleVertexProperties::sep);
            if(splitting.size() != 2)
            {
                LOG_WARNING("During set operations of two Vertices properties, the length was not 2.");
                LOG_WARNING(splitting.join("|"));
            }
            else
             m_properties.insert(splitting[0], splitting[1]);
        }
        return *this;
    }

private:
    string m_id;
    string m_nid;
    QString m_defaultVarFeat;
    QString m_catName;
    QHash<QString, QString> m_properties;
};

class SimpleEdgeProperties
{
public:
    SimpleEdgeProperties(){}
    SimpleEdgeProperties(string id)
        :m_id(id){}

    void setId(string id){m_id = id;}
    string id()const {return m_id;}

    void setDefaultVarFeat(QString featKey){m_defaultVarFeat = featKey;}
    QString defaultVarFeat()const {return m_defaultVarFeat;}

    QHash<QString, QString> featuresStruct() {return m_properties;}
    void setFeaturesStruct(QHash<QString, QString> features){m_properties = features;}

    void setCustomProperties(QString key, QString value){m_properties.insert(key, value);}
    QString customProperties(QString key)const {return m_properties.value(key, "");}
    QSet<QString> setOfProperties()const
    {
        QSet<QString> p;
        QHashIterator<QString, QString> it(m_properties);
        while(it.hasNext())
        {
            it.next();
            p.insert(it.key()+"%SEP%"+it.value());
        }
        return p;
    }

    SimpleEdgeProperties& intersection(QSet<QString> set)
    {
       QSet<QString> inter  = this->setOfProperties();
       QSet<QString> result = inter.intersect(set);
       m_properties.clear();
       foreach(QString p, result)
       {
           QStringList splitting = p.split("%SEP%");
           if(splitting.size() != 2)
           {
               LOG_WARNING("During intersection of two Vertices properties, the length was not 2.");
               LOG_WARNING(splitting.join("|"));
           }
           else
            m_properties.insert(splitting[0], splitting[1]);
       }
       return *this;
    }
private:
    string m_id;
    QString m_defaultVarFeat;
    QHash<QString, QString> m_properties;
};

typedef AbstractGraph<SimpleVertexProperties, SimpleEdgeProperties> SimpleGraph;

/** Define every possible graph in the programm.
  * To handle more type of graphs, feel free to extend the possibilities
  * in genericgraph.h (and so on) and put the new type,
  * in the boost::variant
  */
typedef boost::variant<SimpleGraph> GraphType;

#endif // GENERICGRAPH_H
