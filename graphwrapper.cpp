#include "graphwrapper.h"

#include <QtGui>

GraphWrapper::GraphWrapper(QString filepath)
    :_filepath(filepath)
{
    QString ext = QFileInfo(filepath).completeSuffix();
    if(ext == "dep.xml")
    {
        parser = new DepXMLParser(_graph_type);
        //LOG_INFO("DepXML parser is chosen.");
    }
    else
    {
        LOG_WARNING("File "+filepath+" cannot be recognised as valid, so it cannot be parsed.");
        return;
    }
    qDebug() << filepath;
    parser->read(filepath);
    _graph = boost::get<SimpleGraph>(_graph_type);
    _nodes = _graph.getOrderedVertices();
}

GraphWrapper::GraphWrapper(QString filepath, SimpleGraph &g, unordered_set<string> ids)
    :_filepath(filepath)
{
    for(unordered_set<string>::iterator it = ids.begin(); it != ids.end(); ++it)
    {
        _graph.addVertex(g.properties(g.getVertexByNId(*it)));
    }

    for(unordered_set<string>::iterator it = ids.begin(); it != ids.end(); ++it)
    {
        SimpleGraph::Vertex v = g.getVertexByNId(*it);
        EdgesIterator ei, edge_end;
        for(boost::tie(ei, edge_end) = out_edges(v, g.getGraph());
            ei != edge_end; ++ei)
        {
            try
            {
                SimpleGraph::Vertex src = _graph.getVertexById(g.properties(v).id());
                SimpleGraph::Vertex tar = _graph.getVertexById(g.properties(target(*ei, g.getGraph())).id());
                _graph.addEdge(src, tar, g.properties(*ei));
            }
            catch(std::exception &e){}
        }
    }
    _nodes = _graph.getOrderedVertices();
}

GraphWrapper::~GraphWrapper()
{
}

void GraphWrapper::print()
{
    _graph.pretty_print();
}

GraphWrapper* GraphWrapper::createSubgraph(unordered_set<string> ids)
{
    return new GraphWrapper(_filepath, _graph, ids);
}

QString GraphWrapper::nodeProperty(Node n, QString p)
{
    if(p == "id")
        return QString::fromStdString(_graph.properties(n).id());
    else if(p == "nid")
        return QString::fromStdString(_graph.properties(n).nid());
    else
        return _graph.properties(n).customProperties(p);
}

QString GraphWrapper::nodeProperty(NodesIterator it, QString p)
{
    if(p == "id")
        return QString::fromStdString(_graph.properties(it->second).id());
    else if(p == "nid")
        return QString::fromStdString(_graph.properties(it->second).nid());
    else
        return _graph.properties(it->second).customProperties(p);
}

QString GraphWrapper::nodeProperty(string id, QString p)
{
    if(p == "id")
        return QString::fromStdString(id);
    else if(p == "nid")
        return QString::fromStdString(_graph.properties(_graph.getVertexById(id)).nid());
    else
        return _graph.properties(_graph.getVertexById(id)).customProperties(p);
}

QString GraphWrapper::edgeProperty(EdgesIterator it, QString p)
{
    if(p == "id")
        return QString::fromStdString(_graph.properties(*it).id());
    else
        return _graph.properties(*it).customProperties(p);
}
