#include "grqlsyntaxhighlighter.h"
#include "ogregraphwindow.h"
#include "ui_ogregraphwindow.h"

OGREGraphWindow::OGREGraphWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OGREGraphWindow)
{    
    ui->setupUi(this);    

    g_controller = new GraphController(qobject_cast<GraphsScene*>(ui->graphsWidget->scene()));
    config = Config::getInstance();

    config->setDefaultGeneralisationRules(qApp->applicationDirPath()+"/generalization_depxml.ogf");

    //Accept drag and drop within the tree view for packages
    ui->packagesTree->setDragDropMode(QAbstractItemView::InternalMove);
    ui->packagesTree->setSelectionMode(QAbstractItemView::ExtendedSelection);

    //Typesetting font for rulesTextEdit
    new GrQLSyntaxHighlighter(ui->rulesTextEdit->document());
    ui->rulesTextEdit->setFontFamily("Monaco");

    //Disable generate rules when no selection
    ui->generateRulesAction->setEnabled(false);
    ui->actionSaveAnnotations->setEnabled(false);

    connect(ui->openGraphsAction, SIGNAL(triggered()), this, SLOT(openGraphs()));
    connect(ui->openDirectoryAction, SIGNAL(triggered()), this, SLOT(openDirWithGraphs()));
    connect(ui->generateRulesAction, SIGNAL(triggered()), this, SLOT(generateRules()));
    connect(ui->addPackageBtn, SIGNAL(clicked()), this, SLOT(addPackage()));
    connect(ui->removePackageBtn, SIGNAL(clicked()), this, SLOT(removeTreeItem()));
    connect(ui->viewGraphBtn, SIGNAL(clicked()), this, SLOT(viewGraph()));
    connect(ui->packagesTree, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)), SLOT(viewGraph(QTreeWidgetItem *, int)));

    connect(ui->actionSaveAnnotations, SIGNAL(triggered()), this, SLOT(saveAnnotations()));

    connect(ui->sentencesViewAction, SIGNAL(triggered(bool)), ui->packagesDockWidget, SLOT(setVisible(bool)));
    connect(ui->rulesViewAction, SIGNAL(triggered(bool)), ui->rulesDockWidget, SLOT(setVisible(bool)));
    connect(ui->graphsWidget->scene(), SIGNAL(selectionChanged()), this, SLOT(enableGeneration()));
}

OGREGraphWindow::~OGREGraphWindow()
{
    delete ui;
}

void OGREGraphWindow::addPackage()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("New package's name"),
                                         tr("Name:"), QLineEdit::Normal,
                                         "Package", &ok);
    if (ok && !text.isEmpty())
        ui->packagesTree->addTopLevelItem(new QTreeWidgetItem(QStringList() << text));
}

void OGREGraphWindow::removeTreeItem()
{
    QList<QTreeWidgetItem *> items = ui->packagesTree->selectedItems();
    foreach(QTreeWidgetItem *item, items)
        if(!item->parent())
        {
            qDeleteAll(item->takeChildren());
            delete item;
        }
        else
            delete item;
}

void OGREGraphWindow::viewGraph()
{
    g_controller->clearScene();
    QSet<QTreeWidgetItem *> items = ui->packagesTree->selectedItems().toSet();
    foreach(QTreeWidgetItem * item, items)
    {
        recurseViewGraph(item);
    }
}

void OGREGraphWindow::viewGraph(QTreeWidgetItem* item, int column)
{
    Q_UNUSED(column);
    g_controller->clearScene();
    recurseViewGraph(item);
}

void OGREGraphWindow::recurseViewGraph(QTreeWidgetItem *item)
{
    if(item->childCount() > 0)
    {
        for(int i = 0; i < item->childCount(); i++)
        {
            if(item->child(i)->childCount() > 0)
            {
                recurseViewGraph(item->child(i));
            }
            else
            {
                if(filename2filepath.contains(item->child(i)->text(0)))
                    g_controller->createGraph(filename2filepath.value(item->child(i)->text(0)));
            }
        }
    }
    else
    {
        if(filename2filepath.contains(item->text(0)))
            g_controller->createGraph(filename2filepath.value(item->text(0)));
    }
}

QString OGREGraphWindow::getPackageNameWithDir(QFileInfo fi)
{
    QString nameForPackage = fi.absolutePath().section("/", -1, -1, QString::SectionSkipEmpty);
    if(nameForPackage.isEmpty())
        nameForPackage = "No Package";
    return nameForPackage;
}

void OGREGraphWindow::openGraphs()
{
    QStringList files = QFileDialog::getOpenFileNames(
                this,
                trUtf8("Sélectionnez un ou plusieurs fichiers de description de graphes."),
                config->getDefaultDirectory(),
                "Graphs XML (*.dep.xml *.graph.xml *.xml)");
    if(files.isEmpty())
       return;

    config->setDefaultDirectory(QFileInfo(files[0]).absolutePath());

    QString nameForPackage = getPackageNameWithDir(QFileInfo(files[0]));
    //Search if a package with this name exists
    QList<QTreeWidgetItem*> foundItems = ui->packagesTree->findItems(nameForPackage, Qt::MatchFixedString, 0);
    QTreeWidgetItem* nopackageItem;
    if(foundItems.isEmpty())
        nopackageItem = new QTreeWidgetItem(QStringList() << nameForPackage);
    else
        nopackageItem = foundItems[0];
    ///

    ui->packagesTree->addTopLevelItem(nopackageItem);
    foreach(QString file, files)
    {
        QString filename = QFileInfo(file).fileName();
        filename2filepath.insert(filename, file);
        nopackageItem->addChild(new QTreeWidgetItem(QStringList() << filename));
    }
}

void OGREGraphWindow::openDirWithGraphs()
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    config->getDefaultDirectory(),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);
    if(dir == "")
       return;

    config->setDefaultDirectory(dir);

    QString nameForPackage = getPackageNameWithDir(QFileInfo(dir));
    QTreeWidgetItem *top = new QTreeWidgetItem(QStringList() << nameForPackage);
    ui->packagesTree->addTopLevelItem(top);

    QDirIterator it(dir, QDir::NoDotAndDotDot|QDir::Files|QDir::Dirs, QDirIterator::Subdirectories);
    QTreeWidgetItem* current = top;
    QHash<QString, QTreeWidgetItem*> parents;
    parents.insert(dir, top);
    while(it.hasNext())
    {
        it.next();
        if(it.fileInfo().isDir())
        {
            QDir _dir(it.filePath());
            _dir.cdUp();
            QString filePathParent = _dir.absolutePath();

            QTreeWidgetItem *dir = new QTreeWidgetItem(QStringList() << it.fileInfo().baseName());
            parents.insert(it.filePath(), dir);
            QTreeWidgetItem *p = parents.value(filePathParent);
            p->addChild(dir);
            current = dir;
        }
        else
        {
            QString suffix = it.fileInfo().completeSuffix();
            if(suffix == "dep.xml" || suffix == "graph.xml" || suffix == "xml")
            {
                QString filename = it.fileInfo().fileName();
                filename2filepath.insert(filename, it.filePath());
                current->addChild(new QTreeWidgetItem(QStringList() << filename));
            }
        }
    }
}

void OGREGraphWindow::enableGeneration()
{
    int s = ui->graphsWidget->scene()->selectedItems().size();
    ui->generateRulesAction->setEnabled(s);
    ui->actionSaveAnnotations->setEnabled(s);
}

void OGREGraphWindow::generateRules()
{
    //LOG_INFO("Rules generation in progress...");
    bool ok = false;
    ui->graphsWidget->resetTransform();
    ui->rulesTextEdit->setPlainText(g_controller->generateRule(ok));
    if(!ok)
    {
        QMessageBox::critical(this, trUtf8("Error"), trUtf8("An error occured during the generalization.\n "
                              "Please make sure that the nodes counts are the same and the graphs are isomorph."));
    }
}

void OGREGraphWindow::saveAnnotations()
{
    g_controller->createSelection();
}
